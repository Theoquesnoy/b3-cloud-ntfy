terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-ntfy-vm" {
  name     = "ntfy"
  location = "eastus"
}

resource "azurerm_virtual_network" "vn-ntfy-vm" {
  name                = "ntfy"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-ntfy-vm.location
  resource_group_name = azurerm_resource_group.rg-ntfy-vm.name
}

resource "azurerm_subnet" "s-ntfy-vm" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg-ntfy-vm.name
  virtual_network_name = azurerm_virtual_network.vn-ntfy-vm.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "pb-ntfy" {
  name                = "PublicIpNtfy"
  resource_group_name = azurerm_resource_group.rg-ntfy-vm.name
  location            = azurerm_resource_group.rg-ntfy-vm.location
  allocation_method   = "Dynamic"
}

resource "azurerm_network_interface" "nic-ntfy-vm" {
  name                = "nic-ntfy"
  location            = azurerm_resource_group.rg-ntfy-vm.location
  resource_group_name = azurerm_resource_group.rg-ntfy-vm.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-ntfy-vm.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.pb-ntfy.id
  }
}

resource "azurerm_network_security_group" "ntfy-secu" {
  name                = "SecurityGroupNtfy"
  location            = azurerm_resource_group.rg-ntfy-vm.location
  resource_group_name = azurerm_resource_group.rg-ntfy-vm.name
}

resource "azurerm_network_security_rule" "rules-ntfy" {
  for_each                    = local.nsgrules 
  name                        = each.key
  direction                   = each.value.direction
  access                      = each.value.access
  priority                    = each.value.priority
  protocol                    = each.value.protocol
  source_port_range           = each.value.source_port_range
  destination_port_range      = each.value.destination_port_range
  source_address_prefix       = each.value.source_address_prefix
  destination_address_prefix  = each.value.destination_address_prefix
  resource_group_name         = azurerm_resource_group.rg-ntfy-vm.name
  network_security_group_name = azurerm_network_security_group.ntfy-secu.name

}

resource "azurerm_network_interface_security_group_association" "nic-secu" {
  network_interface_id      = azurerm_network_interface.nic-ntfy-vm.id
  network_security_group_id = azurerm_network_security_group.ntfy-secu.id
}

resource "azurerm_subnet_network_security_group_association" "s-secu" {
  subnet_id                 = azurerm_subnet.s-ntfy-vm.id
  network_security_group_id = azurerm_network_security_group.ntfy-secu.id
}

resource "azurerm_linux_virtual_machine" "vm-ntfy" {
  name                = "vm-ntfy"
  resource_group_name = azurerm_resource_group.rg-ntfy-vm.name
  location            = azurerm_resource_group.rg-ntfy-vm.location
  size                = "Standard_B1s"
  admin_username      = "theo"
  network_interface_ids = [
    azurerm_network_interface.nic-ntfy-vm.id,
  ]

  admin_ssh_key {
    username   = "theo"
    public_key = file("C:/Users/theoq/.ssh/id_rsa_VM.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "Debian"
    offer     = "Debian-10"
    sku       = "10"
    version   = "latest"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "theo"
      private_key = file("C:/Users/theoq/.ssh/id_rsa_VM")
      host        = self.public_ip_address
    }

    inline=[
        "sudo apt install -y gnupg",
        "deb http://ppa.launchpad.net/ansible/ansible/ubuntu bionic main",
        "sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367",
        "sudo apt update",
        "sudo apt install ansible -y",
        "wget https://github.com/binwiederhier/ntfy/releases/download/v2.4.0/ntfy_2.4.0_linux_amd64.deb",
        "sudo dpkg -i ntfy_*.deb",
        "sudo systemctl enable ntfy",
        "sudo systemctl start ntfy"
    ]
  }
}
