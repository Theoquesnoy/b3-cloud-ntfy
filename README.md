# B3-Cloud-ntfy

## Pré-requis

- Télécharger l'application `ntfy`
- Une fois télécharger, cliquer sur le bouton `+` et nommer le sujet `backup_ntfy`
- Cocher `utiliser un serveur différent` et rentrer l'adresse IP public de la VM qui sera déployer avec terraform comme ceci : `http://adresse_ip_vm`

Après avoir cloné le git, mettre le copy.txt dans votre répertoire 'home'

Modifier le `main.tf` pour rajouter votre clé ssh puis exécuter les commandes `terraform init` et ensuite `terraform apply`

Une fois le déploiement fini, récupérer l'adresse IP publique de la VM et noter la

Dans les fichiers ansible, modifier :

- Le fichier `hosts.ini` -> rajouter l'adresse ip publique
- Le fichier `.ssh_config` -> rajouter l'adresse ip et votre clé ssh
- Le fichier `copy.yml` qui se trouve dans `roles/backup/tasks` -> remplacer l'adresse ip par celle de votre VM

Exécuter ensuite le playbook : `ansible-playbook -i hosts.ini playbook/main.yml`
